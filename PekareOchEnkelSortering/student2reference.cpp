#include "student2reference.h"
#include <stdio.h>

const char* nameOfTheStudent2()
{
    return "Dennis Andersson";  // Byt ut denna sträng mot ditt eget namn!
}




// ################ experiment med värde- pekar- och referensanrop #################



struct AnExampleStruct
{
    int _intValue1 = 20;
    int _intValue2;
    float _array[1000];
};


// call by value, theStruct kommer att kopieras (även arrayen)
void printAddresses1(AnExampleStruct theStruct)
{
    printf("\n\n printAddress1\n");
    printf("&theStruct  = %zu\n",   size_t( &theStruct ));
    printf("&theStruct._intValue1  = %zu\n", size_t( &theStruct._intValue1));
    printf("&theStruct._intValue2  = %zu\n", size_t( &theStruct._intValue2));
    printf("&theStruct._array  = %zu\n",  size_t( &theStruct._array));
}


// call by reference, theStruct är identisk med anroparens s.
void printAddresses2(const AnExampleStruct &theStruct)
{
    printf("\n\n printAddress2\n");

    printf("&theStruct  = %zu\n", size_t(&theStruct));
    printf("&theStruct._intValue1  = %zu\n", size_t(&theStruct._intValue1));
    printf("&theStruct._intValue2  = %zu\n", size_t(&theStruct._intValue2));
    printf("&theStruct._array  = %zu\n", size_t(&theStruct._array));
}

// call by pointer. Nu är (*pThestruct) identisk med anroparens s.
void printAddresses3(const AnExampleStruct *pTheStruct)
{
    printf("\n\n printAddress3\n");
    printf("pTheStruct  = %zu\n", size_t( pTheStruct));
    printf("&(*pTheStruct)._intValue1  = %zu\n", size_t( &(*pTheStruct)._intValue1));
    printf("&(*pTheStruct)._intValue2  = %zu\n", size_t( &(*pTheStruct)._intValue2));
    printf("&(*pTheStruct)._array  = %zu\n",  size_t(&(*pTheStruct)._array));
}


// Experimentkoden är färdigskriven med betrakta utskrifterna!!
void experimentMedParameteroverforing()
{

    printf("Experiment med parameteröverföring!\n");
    printf("(studera utskrifterna så att du kan förklara dem i nedanstående kommentar)\n");

    AnExampleStruct s;

    printAddresses1(s);
    printAddresses2(s);
    printAddresses3(&s);

    /* TODO
       Förklaring:
       Först deklarerar vi en struct S som lägger sig längst ner i stacken (efter main), sen skickar in s genom call by value i
       printAdress1 som gör en ren kopia av S som lägger sig längre upp i stacken ovanpå main och där vi har S sparad.
       Den här funktionen dör sedan när den lämnar
       scope och stacken rensas så kvar blir bara den ursprungliga S (och main). I printaddress2 skickar vi sedan denna struct genom
       call by reference. Så nu refererar vi till samma S som vi hade från början i stacken (alltså identisk med anroparens s)
       när vi sedan går in i printaddresses3 så skickar vi addressen som s pekar på, detta är samma address som vi lät s referera
       till i andra funktionen.

       Anledningen till att printaddress1 printar ut address värden som är lägre än i printaddress2 och 3 är för att stacken
       är top down, de vill säga den går från högre address värden till lägre och eftersom printaddress1 kör call by value
       hamnar en kopia längre upp i stacken som resulterar i en lägre minnesaddress.

       Varför  printf("&theStruct  = %zu\n", size_t(&theStruct));
               printf("&theStruct._intValue1  = %zu\n", size_t(&theStruct._intValue1));

               ger samma address är för att datorn sparar en struct i minnet på ett sådant sätt att alla
               medlemmar sparas i ordning som de är deklarerade, objektet som i detta fallet kallas theStruct pekar alltid
               på samma address som den första medlemmen är sparad i

               att addressen sedan ökar med 4 beror på att de bara finns int variabler i structen och en int råkar i detta
               fallet vara 4 byte lång och varje address håller en byte.

               detta kan vi sedan använda för att avgöra hur stor en int är eftersom vi vet att en int är 4 byte och varje
               byte består av 8bitar. 4 * 8 = 32, så en int i det här fallet är ~4000000000
     */
}

void testingCode2()
{

    printf("\n\n\n################ testingCode2 #################\n\n");

    experimentMedParameteroverforing();
}
