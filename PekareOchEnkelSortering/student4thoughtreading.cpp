#include "student4thoughtreading.h"
#include <stdio.h>

// ################ MAGI! Förklara hur tankeläsningen fungerar! #################

void tankPaEttTal()
{
    int x = 108;
    printf( "Denna funktion tanker pa ett hemligt tal (%d)\n", x);
}

void tankelasning()
{
    int  hemligheten;
    printf( "Denna funktion tror att den andra funktionens hemlighet ar %d!\n", hemligheten);
}

void experimentMedTankeoverforing()
{
    printf("Experiment med tankeoverforing!\n");
    printf("(studera utskrifterna sa att du kan forklara dem i kommentaren)\n");
    tankPaEttTal();
    tankelasning();


    /* TODO
       att hemligheten kan gissa rätt beror på att stacken har lämnat skräpvärdet 108 efter sig och inget annat deklareras
       innan vi kommer fram till hemligheten.

       så vad som händer är att vi deklarerar x = 108 som läggs på stacken. Efter vi lämnat scope rensar datorn stacken men 108
       ligger fortfarande kvar i minnet som ett skräpvärde. Så när vi sen deklarerar en oinitierad int kommer den ta upp samma
       minnesdel i stacken som råkade ha kvar värdet 108 i sig.
     */
}

void testingCode4()
{
    printf("\n\n\n################ testingCode4 #################\n\n");

    experimentMedTankeoverforing();
}

