#include "student1.h"
#include <stdio.h> // printf
#include <assert.h>
#include "ragnarstest1.h"
#include "dalgorandom.h"
#include <iostream>


// Läs Detta!

// Pekaruppdragen går ut på att implementera nedastående funktioner
// Många funktioner skall implementeras på två olika sätt (I och P):
// Version I: Räkna ut size och använd arraynotation med hakparanteser och heltalsindex.
// Version P: Använd istället pekarnotation, dvs  *p,  p+=1,  etc.



// ************************************************************
// ANROP:   char* namn = nameOfStudent1();
// UPPGIFT: Returnerar ditt namn!
// ************************************************************
const char* nameOfTheStudent1()
{
    return "Dennis Andersson";  // Byt ut denna sträng mot ditt eget namn!
}




// ################ version I, löses med [ ]  och indexnotation #############



// ************************************************************
// ANROP:   float value = minimumElement(&arr[0], &arr[size]);
// UPPGIFT: Returnerar arrayens minsta värde
// ************************************************************
float minimumElementI(const float *pBegin, const float *pEnd)
{
    // Jag låter min egen lösnin ligga kvar.
    // Förstå denna! Lös de andra uppgifterna själv.

    assert( pBegin < pEnd );

    const long long int size = pEnd - pBegin;
    float extreme = pBegin[0];
    for (int i=0; i<size ; i+=1)
        if (pBegin[i]<extreme)
            extreme = pBegin[i];
    return extreme;
}


// ************************************************************
// ANROP:   float value = maximumElement( &arr[0], &arr[size] );
// UPPGIFT: Returnerar arrayens största värde
// ************************************************************
float maximumElementI(const float *pBegin, const float *pEnd)
{
    assert( pBegin < pEnd );

    const long long int size = pEnd - pBegin;
    float extreme = pBegin[0];

    for (int i = 0; i < size; i++)
    {
        if (pBegin[i] > extreme)
        {
            extreme = pBegin[i];
        }
    }
    return extreme;
}


// ************************************************************
// ANROP:   float value = averageElement( &arr[0], &arr[size] );
// UPPGIFT: Returnerar arrayens medelvärde
// ************************************************************
float averageElementI(const float *pBegin, const float *pEnd)
{
    assert(pBegin < pEnd);

    const long long int size = pEnd - pBegin;
    float sum = 0.0;
    float answer = 0.0;
    int numberOfValues = 0;

    for (int i = 0; i < size; i++)
    {
        sum += pBegin[i];
        numberOfValues++;
    }

    answer = sum / numberOfValues;

    return answer;
}


// ************************************************************
// ANROP:   fillWithRandomI( &arr[0], &arr[size] );
// UPPGIFT: Fyller arrayen med oberoende slumpdata
// ************************************************************
void fillWithRandomI(float *pBegin, float *pEnd)
{
    long long int size = pEnd - pBegin;

    for (int i = 0; i < size; i++)
    {
        pBegin[i] = dalgoRandom() * 10;
    }
}

// ************************************************************
// ANROP:   fillWithRandomI( &arr[0], &arr[size] );
// UPPGIFT: Fyller arrayen med sorterat slumpdata. Minst först
// ************************************************************
void fillWithRandomSortedI(float *pBegin, float *pEnd)
{
    long long int size = pEnd - pBegin;

    pBegin[0] = dalgoRandom() * 10;

    for (int i = 0; i < size - 1; i++)
    {
        pBegin[i + 1] = pBegin[i] + dalgoRandom() * 10;
    }

    // Tips: loopa från vänster till höger, låt varje tal vara
    // likamed föregående tal + ett slumptal >= 0
}


// ************************************************************
// ANROP:   bool contains = containedInArrayI(value,  &arr[0], &arr[size] );
// UPPGIFT: Returnerar true om arrayen innehåller angivet värde
// ************************************************************
bool  containedInArrayI(float value, const float *pBegin, const float *pEnd)
{
    long long int size = pEnd - pBegin;

    for (int i = 0; i < size; i++)
    {
        if(value == pBegin[i])
        {
            return true;
        }
    }
    return false;
}

// ************************************************************
// ANROP:   int number = numberOfValuesEqualToI(value,  &arr[0], &arr[size] );
// UPPGIFT: Returnerar antalet värden som är lika med angivet värde
// ************************************************************
int numberOfValuesEqualToI(float value, const float *pBegin, const float *pEnd)
{
    int equalValues = 0;
    long long int size = pEnd - pBegin;

    for (int i = 0; i < size; i++)
    {
        if(value == pBegin[i])
        {
            equalValues++;
        }
    }
    return equalValues;
}




// ################ version P, samma uppgifter löses nu med pekarnotation  ###############






float minimumElementP(const float *pBegin, const float *pEnd)
{
    // Jag låter min egen lösnin ligga kvar.
    // Förstå denna! Lös de andra uppgifterna själv.

    assert( pBegin < pEnd );
    float extreme = *pBegin;
    for (const float *p=pBegin ; p<pEnd; p+=1)
        if (*p<extreme)
            extreme = *p;
    return extreme;
}


float maximumElementP(const float *pBegin, const float *pEnd)
{
    assert( pBegin < pEnd );

    float extreme = *pBegin;

    for (const float *p = pBegin; p < pEnd; p++)
    {
        if (*p > extreme)
        {
            extreme = *p;
        }
    }
    return extreme;
}

float averageElementP(const float *pBegin, const float *pEnd)
{
    assert( pBegin < pEnd );

    float sum = 0.0;
    int amountOfNumbers = 0;

    for (const float *p = pBegin; p < pEnd; p++)
    {
        sum += *p;
        amountOfNumbers++;
    }
    return sum / amountOfNumbers;
}

void fillWithRandomP(float *pBegin, float *pEnd)
{
    for (float *p = pBegin; p < pEnd; p++)
    {
        *p = dalgoRandom() * 10;
    }
}

void fillWithRandomSortedP(float *pBegin, float *pEnd)
{
    *pBegin = dalgoRandom() * 10;

    for (float *p = pBegin; p < pEnd - 1; p++)
    {
        *(p + 1) = (*p + dalgoRandom() * 10);
    }
}

bool containedInArrayP(float value, const float *pBegin, const float *pEnd)
{
    for (const float *p = pBegin; p < pEnd; p++)
    {
        if (value == *p)
        {
            return true;
        }
    }
    return false;
}


int  numberOfValuesEqualToP(float value, const float *pBegin, const float *pEnd)
{
    int equalValues = 0;

    for (const float *p = pBegin; p < pEnd; p++)
    {
        if (value == *p)
        {
            equalValues++;
        }
    }
    return equalValues;
}




// ################ Skriv din egen testkod här! #################


void studentsTest1()
{
    const int size = 10;
    float value = 2;
    float arr[size] = {1,2,3,4,5,6,7,8,10,2};
    const float *pBegin = &arr[0];
    const float *pEnd = &arr[size];

    float *pBegin1 = &arr[0];
    float *pEnd1 = &arr[size];

    // testa själv dina funktioner här!
    printf("(dina egna tester borde komma har!)\n");

    std::cout << nameOfTheStudent1() << std::endl;

    float minValueI = minimumElementI(pBegin, pEnd);
    std::cout << "minValueI: " << minValueI << std::endl;

    float maxValueI = maximumElementI(pBegin, pEnd);
    std::cout << "maxValueI: " << maxValueI << std::endl;

    float averageValueI = averageElementI(pBegin, pEnd);
    std::cout << "averageValueI: " << averageValueI << std::endl;

    float minValueP = minimumElementP(pBegin, pEnd);
    std::cout << "minValueP: " << minValueP << std::endl;

    float maxValueP = maximumElementP(pBegin, pEnd);
    std::cout << "maxValueP: " << maxValueP << std::endl;

    float averageValueP = averageElementP(pBegin, pEnd);
    std::cout << "averageValueP: " << averageValueP << std::endl;

    fillWithRandomI(pBegin1, pEnd1);
    fillWithRandomSortedI(pBegin1, pEnd1);

    containedInArrayI(value, pBegin, pEnd);
    numberOfValuesEqualToI(value, pBegin, pEnd);

    fillWithRandomP(pBegin1, pEnd1);
    fillWithRandomSortedP(pBegin1, pEnd1);

    containedInArrayP(value, pBegin, pEnd);
    numberOfValuesEqualToP(value, pBegin, pEnd);

}

void testingCode1()
{
   printf("\n\n\n################ testingCode1 ################# \n\n");
   studentsTest1();
}

