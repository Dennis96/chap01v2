#include "student3swaps.h"
#include <stdio.h>
#include "dalgorandom.h"
#include <iostream>

const char* nameOfTheStudent3()
{
    return "Dennis Andersson";  // Byt ut denna sträng mot ditt eget namn!
}


// ################ Implementera swap med pekar- och referensanrop #################



// ANROP:   swap(a,b); eller swap( (&a), (&b) );
// UPPGIFT: Byter värden mellan a och b.
void swap(float* pa, float* pb)
{
    float placeHolder = *pa;
    *pa = *pb;
    *pb = placeHolder;
}

// ANROP:   swap(a,b); eller swap((a), (b));
// UPPGIFT: Byter värden mellan a och b.
void swap(float &a, float &b)
{
    float placeHolder = a;
    a = b;
    b = placeHolder;
}

// För att öva på referenser ger jag här en
// lite annorluna implementerting av max.
float& max(float &a, float &b)
{
    if (a>b)
        return a;
    return b;
}

void testaMax()
{
    float a = dalgoRandom();
    float b = dalgoRandom();
    max(a,b) = 1;
    /* Förklara vad ovanstående kodrad gör!
     * a och b kommer tilldelas ett random värde inom intervallet 0.0 - 1.0. Den variabel som får
     * störst värde kommmer tilldelas värdet 1 efter funktionen max(a,b) returnerar en referens till
     * de värdet mellan a och b som är störst. Säg att a > b då kommer a få värdet 1.
     *
     */
    printf("a=%f, b=%f\n", a,b);
}


void studentsTest3()
{
  // TODO! Skriv din testkod här
    float pa = 5.0;
    float pb = 10.0;
    float a = 2.0;
    float b = 4.0;

    std::cout << "Before swap: " << pa << "\t" << pb << std::endl;
    swap(&pa, &pb);
    std::cout << "After swap: " << pa << "\t" << pb << std::endl;

    std::cout << "Before swap: " << a << "\t" << b << std::endl;
    swap((a), (b));
    std::cout << "After swap: " << a << "\t" << b << std::endl;

    std::cout << "biggest value between a and b is: " << max((a), (b)) << std::endl;
}



void testingCode3()
{

    printf("\n\n\n################ testingCode3 #################\n\n");

    studentsTest3();
    testaMax();
}
