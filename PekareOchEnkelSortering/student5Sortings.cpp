#include "student5Sortings.h"
#include "student1.h"
#include "student3swaps.h"

#include "ragnarstest5.h"
#include <stdio.h> // printf
#include <iostream>


// Läs detta.
// Nedan finns version I och P av bubble- respektive insert-sort.

// Själv har jag implementerat bubbleSortI och insertSortP.
// Två algoritmer återstår!



// ************************************************************
// ANROP:   char* namn = nameOfStudent5();
// UPPGIFT: Returnerar ditt namn!
// ************************************************************
const char* nameOfTheStudent5()
{
    return "Dennis Andersson";  // Byt ut denna sträng mot ditt eget namn!
}



/**********************************************************************************
 * ANROP:   bubbleSortI( pBegin, pEnd);
 * UPPGIFT: pBegin och pEnd definierar en array av element (pBegin pekar på det
 *          första elementet och pEnd EFTER det sista).
 *          Funktionen sorterar arrayen så att det minsta elementet hamnar först.
 *
 *          Algoritmen som används måste vara den version av bubbleSort som
 *          beskrivs i uppdragshäftet!!
 * ********************************************************************************/
void bubbleSortI(float *pBegin, float *pEnd)
{
    long long int size = pEnd - pBegin;
    for (long long int maxIndex = size-1 ; maxIndex>0 ; maxIndex -= 1)
        for (int i=0 ; i<maxIndex ; i+=1)
            if (pBegin[i]>pBegin[i+1])
                swap( (pBegin[i]), (pBegin[i+1]));
}


/**********************************************************************************
 * ANROP:   insertSortI( pBegin, pEnd);
 * UPPGIFT: pBegin och pEnd definierar en array av element (pBegin pekar på det
 *          första elementet och pEnd EFTER det sista).
 *          Funktionen sorterar arrayen så att det minsta elementet hamnar först.
 *
 *          Algoritmen som används måste vara den version av insertSort som
 *          beskrivs i uppdragshäftet!!
 * ********************************************************************************/

void insertSortI(float *pBegin, float *pEnd)
{
    long long int size = pEnd - pBegin;

    for (int i = 1; i < size; i++)
    {
        int x = i;

        for (; x > 0 && pBegin[x] < pBegin[x-1]; x--)
        {
            float temp = pBegin[x-1];

            pBegin[x-1] = pBegin[x];
            pBegin[x] = temp;
        }
    }
}

void bubbleSortP(float *pBegin, float *pEnd)
{
    for (float *pMaxIndex = (pEnd - 1); pMaxIndex > pBegin; pMaxIndex--)
    {
        for (float *pFirstIndex = pBegin; pFirstIndex < pMaxIndex; pFirstIndex++)
        {
            if (*pFirstIndex > *(pFirstIndex + 1))
            {
                swap( (*pFirstIndex), *(pFirstIndex + 1));
            }
        }
    }
}

void insertSortP(float *pBegin, float *pEnd)
{
    for (float *pLastInOrder=pBegin; pLastInOrder<pEnd-1 ; pLastInOrder+=1)
    {
        float x = *(pLastInOrder+1);
        float *p = pLastInOrder;

        for (   ; p>=pBegin && *p>x ; p-=1)
        {
            *(p+1) = *p;
        }

        *(p+1) = x;
    }
}

void studentsTest5()
{

    // testa själv bubble- och insert- sort här!
    printf("dina egna tester borde komma har!\n");
    printf("\n\n");

    const int size = 10;

    float arr[size] = {1,2,3,4,5,6,7,8,10,2};
    float *pBegin = &arr[0];
    float *pEnd = &arr[size];

    float arr2[size] = {2,10,8,7,6,5,4,3,2,1};
    float *pBegin2 = &arr2[0];
    float *pEnd2 = &arr2[size];

    float arr3[size] = {2,10,8,7,6,5,4,3,2,1};
    float *pBegin3 = &arr3[0];
    float *pEnd3 = &arr3[size];

    float arr4[size] = {2,10,8,7,6,5,4,3,2,1};
    float *pBegin4 = &arr4[0];
    float *pEnd4 = &arr4[size];


    std::cout << "Before sort with pointer" << std::endl;
    for (int i = 0; i < size; i++)
    {
        std:: cout << arr[i] << " ";
    }
    std::cout << std::endl;

    bubbleSortP( pBegin, pEnd);

    std::cout << "After sort with pointer" << std::endl;
    for (int i = 0; i < size; i++)
    {
        std:: cout << arr[i] << " ";
    }
    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "Before sort with index" << std::endl;
    for (int i = 0; i < size; i++)
    {
        std:: cout << arr2[i] << " ";
    }

    bubbleSortI( pBegin2, pEnd2);

    std::cout << std::endl;
    std::cout << "After sort with Index" << std::endl;
    for (int i = 0; i < size; i++)
    {
        std:: cout << arr2[i] << " ";
    }

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "Before sort with pointer (insert method)" << std::endl;
    for (int i = 0; i < size; i++)
    {
        std:: cout << arr3[i] << " ";
    }

    insertSortP(pBegin3, pEnd3);

    std::cout << std::endl;
    std::cout << "After sort with pointer (insert method)" << std::endl;
    for (int i = 0; i < size; i++)
    {
        std:: cout << arr3[i] << " ";
    }

    std::cout << std::endl;
    std::cout << std::endl;
    std::cout << "Before sort with index (insert method)" << std::endl;
    for (int i = 0; i < size; i++)
    {
        std:: cout << arr4[i] << " ";
    }

    insertSortI(pBegin4, pEnd4);

    std::cout << std::endl;
    std::cout << "After sort with index (insert method)" << std::endl;
    for (int i = 0; i < size; i++)
    {
        std:: cout << arr4[i] << " ";
    }



    // TODO

}

void testingCode5()
{
    printf("\n\n\n################ testingCode5 #################\n\n");

    studentsTest5();
}
